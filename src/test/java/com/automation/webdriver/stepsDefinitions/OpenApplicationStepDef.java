package com.automation.webdriver.stepsDefinitions;

import com.automation.webdriver.tasks.OpenApplicationTask;
import cucumber.api.PendingException;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;

import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;

public class OpenApplicationStepDef {

    String name;
    @Before
    public void setTheStage() {
        OnStage.setTheStage(new OnlineCast());
    }

    @Given("(.*) ingresa a Afiliación Digital")
    public void deseo_ingresar_a_la_aplicacion_de_retractos(String name){
        this.name = name;
        //openApplicationStep.abre_navegador();
       // throw new PendingException();
    }

    @When("inicia el proceso de afiliación")
    public void ingreso_a_la_pagina_web_de_retractos(){
        theActorCalled(name).attemptsTo(
                OpenApplicationTask.iniciarAfiliacion()
        );
       // openApplicationStep.ingresa_aplicacion(;
       // throw new PendingException();
    }

    @Then("es redireccionado a la pantalla de datos de contacto")
    public void carga_la_pantalla_Home(){
       // openApplicationStep.carga_pantalla();
       // throw new PendingException();
    }
}