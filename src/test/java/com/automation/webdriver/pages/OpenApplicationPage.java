package com.automation.webdriver.pages;

import org.openqa.selenium.By;

public class OpenApplicationPage{
    public static By PANTALLA_HOME = By.xpath("/html/body/app-root/app-root/app-home/div/div/div[1]/div[2]/div/div[1]");
    public static By ESTOY_LISTO = By.className("colfondos-button");
    public static By PANTALLA_DATOS_CONTACTO = By.xpath("/html/body/app-root/app-root/app-content/div/div/div[2]/div/app-viability-terms/div/app-subheading/div/span");


}