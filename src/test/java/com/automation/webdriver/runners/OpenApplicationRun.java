package com.automation.webdriver.runners;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import net.thucydides.core.annotations.Screenshots;
import org.junit.runner.RunWith;

@Screenshots(beforeAndAfterEachStep=true)
@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        plugin = {"pretty"},
        features="src/test/resources/features/openApplicationFeature.feature"
)

public class OpenApplicationRun {
}
