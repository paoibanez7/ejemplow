package com.automation.webdriver.tasks;

import com.automation.webdriver.pages.OpenApplicationPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import static net.serenitybdd.screenplay.Tasks.instrumented;

public class OpenApplicationTask implements Task {

    public static Performable iniciarAfiliacion(){
        return instrumented(OpenApplicationTask.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
    actor.attemptsTo(
            Click.on(OpenApplicationPage.ESTOY_LISTO)
    );
    }
}
